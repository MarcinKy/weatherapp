import axios from 'axios';

export const getWeatherByCityName = cityName => {
 return axios.get(`https://www.metaweather.com/api/location/search/?query=${cityName}`).then(resp => {
   const { woeid } = resp.data[0];
   return axios.get(`https://www.metaweather.com/api/location/${woeid}`).then(resp => resp.data)
 }).catch((err) => {
    console.log("Cos poszło nie tak... "); 
    alert("Sorry, there is no such a place..."); 
    window.location.reload();
  })
}

export const getWeatherByCoords = (latt, long) => {
  return axios.get(`https://www.metaweather.com/api/location/search/?lattlong=${latt},${long}`).then(resp => {
    const { woeid } = resp.data[0];
    return axios.get(`https://www.metaweather.com/api/location/${woeid}`).then(resp => resp.data)
  });
}