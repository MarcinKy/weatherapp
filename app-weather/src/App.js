import React from 'react';
import { getWeatherByCityName, getWeatherByCoords } from './Components/requests';
import './App.css';
import { MdLocationOn } from "react-icons/md"; 
import { MdSearch } from "react-icons/md";

class App extends React.Component {
  constructor() {
    super()
    this.state = {
      searchedInputValue: '',
      cityName: '',
      countryName: '',
      todaysWeatherForecast: {},  
      nextDaysWeatherForecast: [],   
    }
  }
  
   updateInputValue = event => {
    this.setState({searchedInputValue: event.target.value});
  }

  getWeatherByCity = () => {
    const { searchedInputValue: cityName } = this.state;
    getWeatherByCityName(cityName).then(resp => {
      this.displayForecastData(resp);
    })
  }

  getWeatherByLocation = () => {
    if ("geolocation" in navigator) {
      console.log("Available");
      navigator.geolocation.getCurrentPosition(location => {
        const { latitude, longitude } = location.coords;          
        getWeatherByCoords(latitude, longitude).then(resp => {
          this.displayForecastData(resp);
        });
      })
    } else {        
      alert("Localization service is unavailable!");
    }
  }

  displayForecastData = forecastData => {
    const [todaysWeather, ...nextDaysWeather] = forecastData.consolidated_weather;      
           
    this.setState({
      cityName: forecastData.title,
      countryName: forecastData.parent.title,
      todaysWeatherForecast: {
        weatherDate: todaysWeather.applicable_date,
        currentTemp: Math.round(todaysWeather.the_temp),
        maxTemp: Math.round(todaysWeather.max_temp),
        minTemp: Math.round(todaysWeather.min_temp),
        airPress: todaysWeather.air_pressure,
        weatherStateAbbr: todaysWeather.weather_state_abbr,
        weatherStateName: todaysWeather.weather_state_name,          
      },
      nextDaysWeatherForecast: nextDaysWeather,             
    });      
  }

  render() {   
    return (     
      <>
        <nav>      
          <input className="nav-input" onChange={this.updateInputValue} value={this.state.searchedInputValue} placeholder="Type place name..."/>
          <button className="nav-icon" onClick={this.getWeatherByCity}><MdSearch /></button>
          <button className="nav-icon" onClick={this.getWeatherByLocation}><MdLocationOn /></button>                
        </nav>

        <main>
          { this.state.cityName && (
            <div className="main-container">
              <div className="today-weather-container"> 
                <div className="today-weather--basic-info">
                  <h1>{this.state.cityName}</h1>
                  <span><p className="today-weather-country">({this.state.countryName})</p></span>
                  <p className="today-weather-date">today is <span className="highlighted-value">{this.state.todaysWeatherForecast.weatherDate}</span></p>
                </div>
                <div className="today-weather--temp-press">
                  <p className="avg-temp">{this.state.todaysWeatherForecast.currentTemp} &#176;C</p>                
                  <p className="min-max-temp">
                    min: <span className="highlighted-value">{this.state.todaysWeatherForecast.minTemp} &#176;C</span> / 
                    max: <span className="highlighted-value">{this.state.todaysWeatherForecast.maxTemp} &#176;C</span>
                  </p> 
                  <p className={ this.state.todaysWeatherForecast.airPress > 1000 ? 'warning-value' : 'safe-value'}>air pressure: {this.state.todaysWeatherForecast.airPress} hPa</p>
                </div>
                <div className="today-weather--icon">
                  <img src={`https://www.metaweather.com/static/img/weather/${this.state.todaysWeatherForecast.weatherStateAbbr}.svg`} alt={this.state.todaysWeatherForecast.weatherStateName}/>
                </div>
              </div>

              <div className="next-days-weather-container">                        
                {this.state.nextDaysWeatherForecast.map((element, index) => (
                  <div className="next-day-weather-item" key={element.id}>
                    <div className="next-day-weather-item-basic">
                      <p className="highlighted-value">{element.applicable_date}</p>
                      <p className="main-info"> {Math.round(element.the_temp)} &#176;C</p>
                    </div>
                    <p className={ element.air_pressure > 1000 ? 'warning-value' : 'safe-value'}>{element.air_pressure} hPa</p>
                    <img 
                      src={`https://www.metaweather.com/static/img/weather/${element.weather_state_abbr}.svg`}
                      alt={element.weather_state_name} 
                    />
                  </div>
                ))}               
              </div>  
            </div>
          )}
        </main>

      <footer>
        <p>App created using <a href="https://www.metaweather.com/api/">MetaWeather API</a></p>
      </footer>
      </>
    );
  }
}

export default App;
