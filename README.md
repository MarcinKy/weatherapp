# Version 0.2
## List of changes
## 1. Implemented searching by name of location (first matching result).
## 2. Implemented searching by gps location (via geolocation browser object).
## 3. Air pressure is rendered different when is obove 1000 hPa (via conditional formating).
## 4. Error message when there is nothing to diplay (no such a place or zero-length string).
## 5. App is responsive, can be used on moile phone.
